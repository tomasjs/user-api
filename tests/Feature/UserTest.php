<?php

namespace Tests\Feature;

use App\Models\User;
use App\Models\UserDetails;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    public $userFields = [
        'first_name' => 'John',
        'last_name' => 'Doe',
        'email' => 'johnDoe@email.com',
        'password' => 'password',
        'address' => 'Some address, Vilnius, Lithuania'
    ];

    public $newUserFields = [
        'first_name' => 'Tom',
        'last_name' => 'Dom',
        'email' => 'tom@tom.tom',
        'password' => 'tomtomtom',
        'address' => 'Some address, Kaunas'
    ];

    public function test_user_create_required_fields()
    {
        $this->json('POST', 'api/users', ['Accept' => 'aplication/json'])
            ->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'first_name',
                    'last_name',
                    'email',
                    'password'
                ]
            ]);
    }

    public function test_user_create_success()
    {
        $this->json('POST', 'api/users', $this->userFields, ['Accept' => 'aplication/json'])
            ->assertStatus(200)
            ->assertJson([
                'user' => [
                    'first_name' => 'John',
                    'last_name' => 'Doe',
                    'email' => 'johnDoe@email.com',
                    'user_details' => [
                        'address' => 'Some address, Vilnius, Lithuania'
                    ]
                ]
            ]);
    }

    public function test_user_update_required_fields()
    {
        $content = $this->post('api/users', $this->userFields, ['Accept' => 'aplication/json']);

        $id = $content['user']['id'];

        $this->json('PUT', "api/users/$id", ['Accept' => 'aplication/json'])
            ->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'first_name',
                    'last_name',
                    'email',
                    'password'
                ]
            ]);
    }

    public function test_user_update()
    {
        $content = $this->post('api/users', $this->userFields, ['Accept' => 'aplication/json']);

        $id = $content['user']['id'];

        $content = $this->json('put', "api/users/$id", $this->newUserFields, ['Accept' => 'aplication/json'])
            ->assertStatus(200)
            ->assertJson([
                'user' => [
                    'first_name' => 'Tom',
                    'last_name' => 'Dom',
                    'email' => 'tom@tom.tom',
                    'user_details' => [
                        'address' => 'Some address, Kaunas'
                    ]
                ]
            ]);
    }

    public function test_user_delete()
    {
        $content = $this->post('api/users', $this->userFields, ['Accept' => 'aplication/json']);

        $id = $content['user']['id'];

        $this->json('DELETE', "api/users/$id", ['Accept' => 'aplication/json'])
            ->assertStatus(200);

        $this->assertNull(User::find($id));
        $this->assertNull(UserDetails::where('user_id', $id)->first());
    }

    public function test_users_list()
    {
        $this->post('api/users', $this->userFields, ['Accept' => 'aplication/json']);

        $this->json('GET', "api/users", ['Accept' => 'aplication/json'])
            ->assertStatus(200)
            ->assertJsonStructure([
                'users' => [
                    [
                        'id',
                        'first_name',
                        'last_name',
                        'email',
                        'user_details' => [
                            'address'
                        ]
                    ]
                ]
            ]);
    }
}
