<?php

namespace App\Services;

use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Log;
use Throwable;
use Hash;

class UserService
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function create(array $userFields, array $userDetailsFields): User
    {
        $userFields['password'] = Hash::make($userFields['password']);
        $this->user = $this->user->create($userFields);

        if ($userDetailsFields) {
            $this->updateDetails($userDetailsFields);
        }

        return $this->user;
    }

    public function update(array $userFields, array $userDetailsFields, User $user): User
    {
        $userFields['password'] = Hash::make($userFields['password']);
        $this->user = $user;

        $this->user->update($userFields);

        if ($userDetailsFields) {
            $this->updateDetails($userDetailsFields);
        }

        return $this->user;
    }

    private function updateDetails(array $userDetails): bool
    {
        try {
            $this->user->userDetails()->updateOrCreate([], $userDetails);
        } catch (Throwable $ex) {
            Log::debug($ex->getMessage());

            return false;
        }

        return true;
    }

    public function getCurrentUser(): User
    {
        return $this->user;
    }

    public function listAllWithDetails(): Collection
    {
        return $this->user->with('userDetails')->get();
    }

    public function delete(User $user): bool
    {
        return $user->delete();
    }
}
