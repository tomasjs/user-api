<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;

abstract class ApiController extends Controller
{
    public function respondWithData(array $data)
    {
        return $this->respond($data, JsonResponse::HTTP_OK);
    }

    public function respondWithError(string $message = '')
    {
        $data = [
            'message' => $message
        ];

        return $this->respond($data, JsonResponse::HTTP_BAD_REQUEST);
    }

    public function respondWithSuccess(string $message = '')
    {
        $data = [
            'message' => $message
        ];

        return $this->respond($data, JsonResponse::HTTP_OK);
    }

    private function respond(array $data, int $code)
    {
        return response()->json($data, $code);
    }
}
