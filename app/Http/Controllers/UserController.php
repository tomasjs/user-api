<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserDeleteRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Services\UserService;
use App\Models\User;

class UserController extends ApiController
{
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function store(UserCreateRequest $request)
    {
        $userFields = $request->only(['first_name', 'last_name', 'email', 'password']);
        $userDetailsFields = $request->only(['address']);

        $user = $this->userService->create($userFields, $userDetailsFields);

        return $this->respondWithData(['user' => $user->load('userDetails')]);
    }

    public function update(UserUpdateRequest $request, User $user)
    {
        $userFields = $request->only(['first_name', 'last_name', 'email', 'password']);
        $userDetailsFields = $request->only(['address']);

        $updatedUser = $this->userService->update($userFields, $userDetailsFields, $user);

        return $this->respondWithData(['user' => $updatedUser->load('userDetails')]);
    }

    public function destroy(UserDeleteRequest $request, User $user)
    {
        $this->userService->delete($user);

        return $this->respondWithSuccess('Deleted');
    }

    public function index()
    {
        $users = $this->userService->listAllWithDetails();

        return $this->respondWithData(['users' => $users]);
    }
}
